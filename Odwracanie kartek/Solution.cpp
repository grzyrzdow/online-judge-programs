#include <stdio.h>

int main()
{
	int i,x,d,n,allZeros,allOnes,firstOne,firstZero,two = 0;
	int papers[10000];
	int results[10];
	
	scanf("%d",&d);	
	for(x = 0 ; x <d ; x++)
	{
		allZeros = 1;
		allOnes = 1;
		firstOne = 0;
		firstZero = 0;
		two = 1;
		scanf("%d",&n);
		
		for(i = 0 ;  i < n ; i++)
		{
			scanf("%d", &papers[i]);
			if(papers[0] == 0 && firstZero == 0)
			{
				firstZero = 1;
			}
			else if (papers[0] == 1 && firstOne == 0)
			{
				firstOne = 1;
			}
			if(papers[i] == 1 && firstOne == 1 && i != 0)
			{
				two = 0;
			}
			if(papers[i] == 0 && firstZero == 1 && i != 0)
			{
				two = 0;
			}
			if(papers[i] == 1)
				allZeros = 0;
			else
				allOnes = 0;
		}
		if(allZeros == 1)
		{
			results[x] = 0;
		}
		else if(allOnes == 1)
		{
			results[x] = 1;
		}
		else if(two == 1 && firstOne == 1)
		{
			results[x] = 2;
		}
		else if(two == 1 && firstZero == 1)
		{
			results[x] = 1;
		}
		else
		{
			results[x] = -1;
		}
		
	}
	for(i = 0 ; i < d ; i++)
	{
		if (results[i] == -1)
			printf("NIGDY\n");
		else
			printf("%d\n",results[i]);
	}
}