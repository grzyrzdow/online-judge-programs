results = []
for i in range(int(input())):
	input() # just to  fit in judge input restrictions
	paperRotations = [int(i) for i in input().split()]
	firstRotation = paperRotations[0]
	previousRotation = paperRotations[1] if len(paperRotations) > 1 else firstRotation # one paper case
	success = True
	for rotation in paperRotations[2:]:
		if previousRotation != rotation:
			success = False
			break
		previousRotation = rotation

	result = -1
	if success:
		if firstRotation == previousRotation:
			if firstRotation == 0:
				result = 0
			else:
				result = 1
		else:
			if firstRotation == 0:
				result = 1
			else:
				result = 2
	results.append(result)

print('\n'.join((str(result) if result != -1 else 'NIGDY') for result in results))