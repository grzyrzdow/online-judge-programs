#include <stdio.h>
#include <stack>

using namespace std;

int main()
{
	int d,i,j,n,r,h,sum,r1,max,h1,r2,h2,sum1 = 0;
	int results[10];
	stack< pair<int,int> > s;
	stack< pair<int,int> > prev;
	scanf("%d",&d);
	for(i = 0 ; i < d ; i++)
	{
		scanf("%d",&n);
		for(j = 0 ; j < n ; j++)
		{
			scanf("%d %d",&r,&h);
			s.push(make_pair(r,h));
		}
		sum = 0;
		max = 0;
		r1 = 1000000;
		h1 = 0;
		while(!prev.empty())
		{
			prev.pop();
		}
		while(!s.empty())
		{
			if(s.top().first > r1)
			{
				if(s.top().first <= max)
				{
					r2 = prev.top().first;
					h2 = prev.top().second;
					prev.pop();
					sum1 = h2;
					while(true)
					{
						if(s.top().first <= r2 && sum1 - h2 < s.top().second)
						{
							sum += s.top().second - sum1 + h2;
							prev.push(make_pair(r2,h2));
							prev.push(make_pair(s.top().first,s.top().second));
							break;
						}
						else if (s.top().first > r2 && sum1 < s.top().second)
						{
							r2 = prev.top().first;
							h2 = prev.top().second;
							prev.pop();
							sum1 += h2;
						}
						else
						{
							while(s.top().first > r2)
							{
								r2 = prev.top().first;
								h2 = prev.top().second;
								prev.pop();
								sum1 += h2;
							}
							prev.push(make_pair(r2,h2));
							prev.push(make_pair(s.top().first,sum1-h2));
							break;
						}
					}
				}
				else if(s.top().second > sum)
				{
					sum = s.top().second;
				}
			}	
			else
			{
				sum += s.top().second;
				prev.push( make_pair(s.top().first , s.top().second) );
			}	
			if(s.top().first > max)
			{
				max = s.top().first;
				while(!prev.empty())
				{
					prev.pop();
				}
				prev.push( make_pair(max,s.top().second) );
			}		
			r1 = s.top().first;	
			h1 = s.top().second;	
			s.pop();
		}
		results[i] = sum;
	}
	for(i = 0 ; i < d ; i++)
	{
		printf("%d\n",results[i]);
	}
}