#include <iostream>

using namespace std;

int money[5];
int values[5] = {10, 20, 50, 100, 200};

bool check(int k)
{
	int start = k;
	int currentTier = 4;
	while(currentTier >= 0 && k > 0)
	{
		if(money[currentTier] > 0 && k >= values[currentTier])
		{
			int amount = k/values[currentTier];
			if(values[currentTier] != 50)
			{
				if(money[currentTier] >= amount)
				{
					k -= values[currentTier] * amount;
				}
				else
				{
					k -= values[currentTier] * money[currentTier];
				}
			}
			else
			{
				if(money[currentTier] >= amount)
				{
					if(amount%2 == 1)
					{
						k -= values[currentTier] * (amount - 1);
					}
					else
					{
						k -= values[currentTier] * amount;
					}
				}
				else
				{
					if(money[currentTier]%2 == 1)
					{
						k -= values[currentTier] * (money[currentTier] - 1);
					}
					else
					{
						k -= values[currentTier] * money[currentTier];
					}
				}
			}			
		}
		currentTier--;
	}
	if(k == 0)
		return true;
	currentTier = 4;
	k = start;
	while(currentTier >= 0 && k > 0)
	{
		if(money[currentTier] > 0 && k >= values[currentTier])
		{
			int amount = k/values[currentTier];
			if(values[currentTier] != 50)
			{
				if(money[currentTier] >= amount)
				{
					k -= values[currentTier] * amount;
				}
				else
				{
					k -= values[currentTier] * money[currentTier];
				}
			}
			else
			{
				if(money[currentTier] >= amount)
				{
					if(amount%2 == 1)
					{
						k -= values[currentTier] * amount;
					}
					else
					{
						k -= values[currentTier] * (amount - 1);
					}
				}
				else
				{
					if(money[currentTier]%2 == 1)
					{
						k -= values[currentTier] * money[currentTier];
					}
					else
					{
						k -= values[currentTier] * (money[currentTier] - 1);
					}
				}
			}			
		}
		currentTier--;
	}
	if(k == 0)
		return true;
	else
		return false;
}

int main()
{
	int d, k = 0;
	bool results[1000];
	
	cin >> d;
	
	for(int i = 0 ; i < d ; i++)
	{
		cin >> money[0] >>  money[1] >>  money[2] >>  money[3] >> money[4] >> k;
		
		if(k%10 == 0)
			results[i] = check(k);
		else
			results[i] = false;			
	}
	
	for(int i = 0 ; i < d ; i++)
	{
		if(results[i])
			cout << "TAK" << endl;
		else
			cout << "NIE" << endl;
	}
}