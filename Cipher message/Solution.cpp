#include <stdio.h>
#include <stack>
#include <cstring>
using namespace std;

	
int main()
{
	char ciphered[200001],results[200001];
	stack<char> letters;
	int n = 0;
	
	scanf("%s",ciphered);
	
	letters.push(ciphered[0]);
	n = strlen(ciphered);	
			
	for(int i = 1 ; i < n ; i++)
	{
		if(!letters.empty() && ciphered[i] == letters.top())
		{
			letters.pop();
		}
		else
		{
			letters.push(ciphered[i]);
		}
	}
	
	n = letters.size();
	
	for(int i = 0 ; i < n ; i++)
	{
		results[n - 1 - i] = letters.top();
		letters.pop();
	}
	printf("%s",results);
}