    #include <stdio.h>
    #include <queue>
    #include <vector>
    #include <cmath>
         
    using namespace std;
         
    int results[182][182];
     
    void Bfs(pair<int,int> p, int n, int m)
    {
    	queue< pair<int,int> > s;
    	s.push(p);
    	while(!s.empty())
    	{
    		int x = s.front().first;
    		int y = s.front().second;
    		s.pop();
    		
    		for(int i = -1 ; i < 2 ; i++)
    		{
    			for(int j = -1 ; j < 2 ; j++)
    			{
    				if(abs(i) != abs(j) && y + j >= 0 && y + j < m && x + i >= 0 && x + i < n && results[x][y] + 1 < results[x + i][y + j])
    				{
    					results[x + i][y + j] = results[x][y] + 1;
    					s.push(make_pair(x + i , y + j));
    				}
    			}
    		}
    	}
    }
     
    int main()
    {
    	int d, n, m = 0;
    	scanf("%d",&d);
    	
    	for(int i = 0 ; i < d ; i++)
    	{
    		scanf("%d",&n);
    		scanf("%d",&m);
    		int map[182][182];
    		char c;
    		vector< pair<int,int> > ones;
    		getchar();
    		for(int x = 0 ; x < n ; x++)
    		{
    			for(int y = 0 ; y < m ; y++)
    			{		
    				c = getchar();		
    				c == '1' ? map[x][y] = 1 : map[x][y] = 0;
    				results[x][y] = n + m - 2; //Maximum distance possible
    				if(map[x][y] == 1)
    				{
    					results[x][y]=0;
    					ones.push_back(make_pair(x,y));
    				}
    			}
    			getchar();
    		}
    		
    		for(int x = 0 ; x < ones.size() ; x++)
    		{
    			Bfs(ones.at(x), n, m);
    		}
    		for(int x = 0 ; x < n ; x++)
    		{
    			for(int y = 0 ; y < m ; y++)
    			{
    				printf("%d ",results[x][y]);					
    			}
    			printf("\n");
    		}
    	}
    } 