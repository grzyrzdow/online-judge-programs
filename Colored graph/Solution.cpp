#include<bits\stdc++.h> 
typedef long long ll;
using namespace std; 
 
const int N = 200+5;
bool en[N][3]={0};
bool cur[2][N][3]={0};
bool curp=0;
 
struct edge
{
	int a;
	int b;
	int c;
	bool e;
}e[40000+5];
 
int main()  
{
	int n,m;
	int i,j,k;
	cin>>n>>m;
	for(i=0;i<m;i++){
		cin>>e[i].a>>e[i].b>>e[i].c;
		e[i].c--;
		e[i].e=1;
	}
	for(i=0;i<3;i++){
		cur[curp][1][i]=1;
		en[1][i]=1;
	}
	int l=0;
	bool end=0,finish=0;
	while(!end && !finish){
		end=1;
		l++;
		for(i=0;i<m;i++) if(e[i].e)
			if(cur[curp][e[i].a][e[i].c]){
				for(k=0;k<3;k++) if(e[i].c!=k && !en[e[i].b][k]){
					cur[!curp][e[i].b][k]=1;
					en[e[i].b][k]=1;
					e[i].e=0;
					if(e[i].b==n){
						finish=1;
						break;
					}}
				end=0;		
			}
		memset(cur[curp],0,sizeof(cur[curp]));
		curp=!curp;
	}
	if(finish) cout<<l;
	else cout<<-1;
	return 0;
}
