#include <stdio.h>

int Amount(int size, int rooms[])
{
	int i, j, done, n = 0;
	n = size;
	int roomsCount[10000];
	for (i = 0; i < size; i++)
	{
		roomsCount[i] = 0;
	}
	for (i = 0; i < size; i++)
	{
		roomsCount[rooms[i]]++;
	}
	for (i = 0; i < size; i++)
	{
		done = 1;
		for (j = 0; j < size; j++)
		{
			if (roomsCount[j] == 0)
			{
				roomsCount[rooms[j]]--;
				rooms[j] = -1;
				roomsCount[j] = -1;
				done = 0;
				n--;
			}
		}
		if (done == 1)
			return n;
	}
	return n;
}

int main()
{
	int rooms[10000];
	int places[10];
	int i, j, n, d, x = 0;

	scanf("%d", &d);

	for (i = 0; i < d; i++)
	{
		scanf("%d", &n);

		for (j = 0; j < n; j++)
		{
			scanf("%d", &x);
			rooms[j] = x - 1;
		}
		places[i] = Amount(n, rooms);
	}
	for (i = 0; i < d; i++)
		printf("%d\n", places[i]);
}