def getFinalRoomsAmount(rooms: list) -> int:
	roomsCount = len(rooms)
	roomPathCounts = [0] * roomsCount
	for destinationRoom in rooms:
		roomPathCounts[destinationRoom] += 1
	results = roomsCount
	for i in range(roomsCount):
		done  = True
		for room, destinationRoom in enumerate(rooms):
			if roomPathCounts[room] == 0:
				roomPathCounts[destinationRoom] -= 1
				results -= 1
				roomPathCounts[room] = -1
				done = False
		if(done):
			return results
	return results

results = []
for i in range(int(input())):
	input() #just to  fit in judge input restrictions
	rooms = [int(destination) - 1 for destination in input().split()]
	results.append(getFinalRoomsAmount(rooms))

print('\n'.join(str(count) for count in results) + '\n')