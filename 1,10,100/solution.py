from math import sqrt

results = []
for i in range(int(input())):
    testVal = int(input())
    results.append(1 if (0.5 + sqrt(2 * testVal - 1.75)).is_integer() else 0) ; 

print(' '.join(str(e) for e in results))