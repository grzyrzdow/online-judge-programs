#include <stdio.h>
#include <cmath>
#include <stack>

using namespace std;

int labirynth[1000][1000];
int maximum;
pair<int, int> second;

void Dfs(pair<int, int> p, int n, int m, int visited)
{
	stack<pair<int, int>> s;
	stack<int> counters;
	s.push(p);
	int counter = 0;
	while (!s.empty())
	{
		bool found = false;
		int x = s.top().first;
		int y = s.top().second;
		s.pop();
		int old = labirynth[x][y];
		labirynth[x][y] = visited;
		for (int i = -1; i < 2; i++)
		{
			for (int j = -1; j < 2; j++)
			{
				if (abs(i) != abs(j) && y + j >= 0 && y + j < m && x + i >= 0 && x + i < n && labirynth[x + i][y + j] < visited && labirynth[x + i][y + j] >= 0)
				{
					s.push(make_pair(x + i, y + j));
					if (found)
					{
						counters.push(counter);
					}
					else
					{
						counter++;
					}
					found = true;
				}
			}
		}
		if (!found)
		{
			if (counter > maximum)
			{
				maximum = counter;
				second = make_pair(x, y);
			}
			if (!counters.empty())
			{
				counter = counters.top();
				counters.pop();
			}
		}
	}
}

int main()
{
	int t, n, m = 0;
	int results[100];
	pair<int, int> zero;
	scanf("%d", &t);

	for (int i = 0; i < t; i++)
	{
		maximum = 0;
		scanf("%d", &m);
		scanf("%d", &n);

		getchar();
		for (int x = 0; x < n; x++)
		{
			for (int y = 0; y < m; y++)
			{
				int v;
				char c = getchar();
				c == '.' ? v = 0 : v = -1;
				labirynth[x][y] = v;
				if (labirynth[x][y] == 0)
				{
					zero = make_pair(x, y);
				}
			}
			getchar();
		}
		second = make_pair(-1, -1);
		Dfs(zero, n, m, 1);
		if (second.first > 0)
			Dfs(second, n, m, 2);
		results[i] = maximum;
	}
	for (int i = 0; i < t; i++)
	{
		printf("Maximum rope length is %d.\n", results[i]);
	}
}